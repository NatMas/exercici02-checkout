import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router,NavigationExtras} from '@angular/router';

@Component({
  selector: 'app-metodopago',
  templateUrl: './metodopago.page.html',
  styleUrls: ['./metodopago.page.scss'],
})
export class MetodopagoPage implements OnInit {

  Card=""
  CardHolder=""
  ExpM=""
  ExpY=""
  CVV=""
  data : any

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;  
      }
    });
   }

  ngOnInit() {
  }

  continuar(){
    
    if(this.Card!=="" 
    && this.CardHolder!==""
    && this.ExpM!==""
    &&this.ExpY!==""
    && this.CVV!==""){

      var nombre = this.data.FName
      var apellido = this.data.LName
      var direcction = this.data.Direccion
      var direccion2 = this.data.Direccion2

      var numero = this.data.Number
      var correo = this.data.Email
      var empresa = this.data.Empresa 
      var pais = this.data.Pais 
      var ciudad = this.data.Ciudad 
      var provincia = this.data.Provincia 
      var cop = this.data.CodigoPostal 
      var envio = this.data.envio;
      var Card=this.Card
      var CardHolder=this.CardHolder
      var ExpM=this.ExpM
      var ExpY=this.ExpY
      var CVV=this.CVV

      class InformacionTotal{
        FName: string = nombre
        LName: string = apellido
        Direccion: string = direcction
        Direccion2: string = direccion2
        Number : string = numero
        Email : string = correo
        Empresa : string = empresa
        Pais : string = pais
        Ciudad : string = ciudad
        Provincia : string = provincia
        CodigoPostal : string = cop
        envio : string = envio
        Card: string = Card
        CardHolder : string = CardHolder
        ExpM : string = ExpM
        ExpY : string = ExpY
        CVV : string = CVV
        constructor( ) {}
      }
  
      let navigationExtras: NavigationExtras = {
        state: {
          parametros: new InformacionTotal,
        }
      };
      this.router.navigate(['final'],navigationExtras);

    }else{
      alert("Faltan campos")
    }



  }
  tornar(){
    this.router.navigate(['direccion'],this.data);

  }

}
