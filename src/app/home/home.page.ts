import { Component } from '@angular/core';
import {NavigationExtras,Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  Correo ='';
  Nombre='';
  Apellido='';
  Empresa = '';
  Direccion ='';
  Direccion2 = '';
  Pais = '';
  Ciudad = '';
  Provincia = '';
  cp = '';
  Telefono = '';

 



  constructor(private router: Router) {}

  logForm(){

    if(this.Nombre!=="" && this.Apellido!==""&& this.Direccion!==""
    &&this.Correo!=="" && this.Telefono!=="" && this.Ciudad!==""){
      var nombre = this.Nombre
      var apellido = this.Apellido
      var direcction = this.Direccion
      var direcction2 = this.Direccion2
      var numero = this.Telefono
      var correo = this.Correo
      var empresa = this.Empresa 
      var pais = this.Pais 
      var ciudad = this.Ciudad 
      var provincia = this.Provincia 
      var cop = this.cp 

      class InfoUser{
        FName: string = nombre
        LName: string = apellido
        Direccion: string = direcction
        Direccion2 : string = direcction2
        Number : string = numero
        Email : string = correo
        Empresa : string = empresa
        Pais : string = pais
        Ciudad : string = ciudad
        Provincia : string = provincia
        CodigoPostal : string = cop
        constructor( ) {}
      }
  
        let navigationExtras: NavigationExtras = {
        state: {
          parametros: new InfoUser,
        }
      };
  
      this.router.navigate(['direccion'],navigationExtras);
    }else{
      alert("Faltan campos")
    }
  }

}

