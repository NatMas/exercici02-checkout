import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-final',
  templateUrl: './final.page.html',
  styleUrls: ['./final.page.scss'],
})
export class FinalPage implements OnInit {
  data : any
  //User
  direccio : string = "Direccio"
  direccio2 : string = "Direccio 2 (Opcional)"
  nom : string ="Nom"
  cognom : string = "Cognom"
  tel: string = "Telefon"
  email : string = "Email"
  empresa : string = "Empresa (Opcional)"

  //Card
  card: string = "Card"
  cardholder : string = "Card holder"
  expMY : string = " Exp.Month + Exp.Year"
  CVV : string = "CVV"

    constructor(private route: ActivatedRoute, private router: Router) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.data = this.router.getCurrentNavigation().extras.state.parametros;  

          this.direccio = "Direcció: "+this.data.Direccion +", Ciutat: "+this.data.Ciudad +", CP: "+this.data.CodigoPostal+", Provincia: "+this.data.Provincia+", Pais: "+this.data.Pais;
          this.nom = "Nom: "+this.data.FName;
          this.cognom= "Cognom: "+this.data.LName;
          this.tel = "Tel: "+this.data.Number;
          this.email = "Email: "+this.data.Email;
          if(this.data.Direccion2!==undefined){
            this.direccio2 = "Dirrecció 2: "+this.data.Direccion2;
          }
          if(this.data.Empresa!==undefined){
            this.empresa= "Empresa: "+this.data.Empresa
          }

          this.card = "Card: "+this.data.Card;
          this.cardholder = "CardHolder: "+this.data.CardHolder;
          this.expMY = "Exp.Month: "+this.data.ExpM +", Exp.Year: "+this.data.ExpY
          this.CVV = "CVV: "+this.data.CVV
        
        }
      });
   }

  ngOnInit() {
  }
  tornarinici(){
    this.router.navigate(['home']);
  }

}
