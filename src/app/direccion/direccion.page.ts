import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router,NavigationExtras} from '@angular/router';

@Component({
  selector: 'app-direccion',
  templateUrl: './direccion.page.html',
  styleUrls: ['./direccion.page.scss'],
})
export class DireccionPage implements OnInit {
  data : any;
  envio : any;

  nomcognom : string = " Nom i Cognom"
  adress : string = "Adreça"
  telefon : string = "Telefon"

  constructor(private route: ActivatedRoute, private router: Router) {
    
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.nomcognom = this.data.FName +" "+this.data.LName;
        this.adress = this.data.Direccion +" "+this.data.Ciudad +" "+this.data.CodigoPostal;
        this.telefon = this.data.Number;
        
    
      }
    });
   }

  ngOnInit() {
  }

  continuar(){
    var envio = this.envio;

    if(envio!==""){
      var nombre = this.data.FName
      var apellido = this.data.LName
      var direcction = this.data.Direccion
      var direccion2 = this.data.Direccion2

      var numero = this.data.Number
      var correo = this.data.Email
      var empresa = this.data.Empresa 
      var pais = this.data.Pais 
      var ciudad = this.data.Ciudad 
      var provincia = this.data.Provincia 
      var cop = this.data.CodigoPostal 

      class InfoUseryenvio{
        FName: string = nombre
        LName: string = apellido
        Direccion: string = direcction
        Direccion2: string = direccion2
        Number : string = numero
        Email : string = correo
        Empresa : string = empresa
        Pais : string = pais
        Ciudad : string = ciudad
        Provincia : string = provincia
        CodigoPostal : string = cop
        envio : string = envio
        constructor( ) {}
      }
  
      let navigationExtras: NavigationExtras = {
        state: {
          parametros: new InfoUseryenvio,
        }
      };

      this.router.navigate(['metodopago'],navigationExtras);
    }
  }
  tornar(){
    this.router.navigate(['home']);

  }

}
